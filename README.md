# Black Knight #

Our game is about a knight going on a quest to discover what has happened in a far away castle

### How do I get set up? ###

* Use the following link: http://lucky-wheel-2020.herokuapp.com/game/
* You can use the movements of your phone in order to move your character around the map
* The game has a quest system
* When you reach a quest item, you will be notified and the share system sends a message to our Slackbot
* As the game loses visibility, this is taken into account in order to be able to progress through the game
